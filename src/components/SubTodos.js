import React from "react";
import { Form } from "./Form";

export const SubTodos = ({ todos = [], onRemoveSubTodo, editSubTodo }) => (
  todos?.length > 0 &&
  <ul className="mt-3 px-3">
    {todos.map((todo, key) => (
      <li
        className={`d-flex align-items-center justify-content-between border border-1 p-1 px-2 mb-1 rounded-3 ${todo.done ? 'border-warning' : ''}`}
        key={key}
      >
        {/* <Form onSubmit={(title) => editSubTodo(todo, title)} default_value={todo.title} /> */}
        <small>{todo.title}</small>

        <div>
          <button
            className="btn btn-outline-danger btn-sm"
            onClick={() => onRemoveSubTodo(key)}
          >
            &times;
          </button>
        </div>
      </li>
    ))}
  </ul>
)