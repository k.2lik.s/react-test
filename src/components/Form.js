
import React, { useState } from 'react'

export const Form = ({ onSubmit, onHandler, default_value = '', placeholder, className = '' }) => {
  const [input_value, setInputValue] = useState('')

  const submitHandler = event => {
    event.preventDefault()

    if (input_value.trim()) {
      onSubmit(input_value)
      setInputValue('')
    }
  }

  return (
    <form onSubmit={submitHandler} className={className}>
      <div className="form-group">
        <input
          type="text"
          className="form-control border-0"
          placeholder={placeholder}
          value={input_value || default_value}
          onChange={e => {
            setInputValue(e.target.value)
            // onHandler(e.target.value.trim())
          }}
        />
      </div>
    </form>
  )
}