import React from "react";

export const Meta = ({ metas = [], onRemoveMeta }) => (
  metas?.length > 0 &&
  <ul className="d-flex align-items-center mt-3 px-3">
    {metas.map((meta, key) => (
      <li
        className={`d-flex align-items-center justify-content-between border border-primary border-1 ps-2 mb-2 me-3 rounded-3 alert-primary`}
        key={key}
      >
        <small className="me-3">{meta.title}</small>

        <div>
          <button
            className="btn btn-light btn-sm"
            onClick={() => onRemoveMeta(key)}
          >
            &times;
          </button>
        </div>
      </li>
    ))}
  </ul>
)