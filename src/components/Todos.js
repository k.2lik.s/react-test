import React from "react";
import { Form } from "./Form";
import { Meta } from "./Meta";
import { SubTodos } from "./SubTodos";

export const Todos = (
  {
    todos,
    onRemove,
    onDone,
    updateTitle,
    onSubTodo,
    onRemoveSubTodo,
    onMeta,
    onRemoveMeta,
    dragStartHendler,
    dragEndHandler,
    dragOverHandler,
    dropHandler,
    sortList
  }) => {

  return (
    <div>
      {[...todos].sort(sortList).map((todo, key) => (
        <div
          onDragStart={(e) => dragStartHendler(e, todo)}
          onDragLeave={(e) => dragEndHandler(e)}
          onDragEnd={(e) => dragEndHandler(e)}
          onDragOver={(e) => dragOverHandler(e)}
          onDrop={(e) => dropHandler(e, todo)}
          draggable={true}
          className={`row  border border-2 p-2 pb-0 mb-2 rounded-3 ${todo.done ? 'border-warning' : ''}`}
          key={todo.id || key}
        >
          <div className="col-md-4 mb-2">
            <Form onSubmit={(title) => updateTitle(todo, title)} default_value={todo.title} />
          </div>

          <div className="col-md-4 mb-2 d-flex align-items-center">
            <small className="me-3">Подзадача: </small>
            <Form onSubmit={(title) => onSubTodo(todo, title)} placeholder="Добавить подзадачу" className="w-100 border border-1 rounded-3" />
          </div>

          <div className="col-md-2 mb-2 d-flex align-items-center">
            <Form onSubmit={(title) => onMeta(todo, title)} placeholder="Добавить метку" className="w-100 border border-1 rounded-3" />
          </div>
          <div className="col-md-2 mb-2 d-flex align-items-center justify-content-end">
            <button
              className={`btn btn-${!todo.done ? 'success' : 'warning'} btn-sm me-2`}
              onClick={() => onDone(todo)}
            >
              {!todo.done ? 'В работу' : 'Отменить'}
            </button>
            <button
              className="btn btn-outline-danger btn-sm"
              onClick={() => onRemove(todo.id)}
            >
              &times;
            </button>
          </div>
          <SubTodos todos={todo?.todos} onRemoveSubTodo={(key) => onRemoveSubTodo(todo, key)} />
          <Meta metas={todo?.metas} onRemoveMeta={(key) => onRemoveMeta(todo, key)} />
        </div>
      ))}
    </div>
  )
}