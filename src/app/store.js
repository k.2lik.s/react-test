import { configureStore } from '@reduxjs/toolkit';
import * as reducers from '../toolkit';

export const store = configureStore({
  reducer: reducers,
});
