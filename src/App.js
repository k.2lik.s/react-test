import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form } from './components/Form';
import { Todos } from './components/Todos';
import {
  addTodo,
  setTodos,
  removeTodo,
  doneTodo,
  titleTodo,
  addSubTodo,
  removeSubTodo,
  addMeta,
  removeMeta,
  searchTodo,
  dragAndDrop
} from "./toolkit/listSlice";

// const url = process.env.DB_URL
const url = 'https://react-test-a2a33-default-rtdb.europe-west1.firebasedatabase.app'

function App() {
  const state = useSelector((state) => ({
    todos: state.default.list
  }))
  const { todos } = state
  const dispatch = useDispatch()
  const [user, setUser] = useState('')
  const [title, setTitle] = useState('Тестовый проект')
  const [current, setCurrent] = useState(null)

  useEffect(() => {
    if (sessionStorage.getItem('user')) {
      setUser(sessionStorage.getItem('user'))
    }
    // eslint-disable-next-line 
  }, [])

  useEffect(() => {
    getTodos()
    // eslint-disable-next-line 
  }, [user])

  useEffect(() => {
    document.title = title
  }, [title])

  // const getUser = async () => {
  //   await axios.get(`${url}/todos.json`, todo)
  //     .then(() =>
  //       dispatch(addTodo(todo))
  //     ).then(() => getTodos())
  // }

  const getTodos = async () => {
    let result = {}
    await axios.get(`${url}/todos.json`)
      .then((res) => {
        if (res.status === 200 && res.data) {
          result = Object.keys(res.data).map(key => {
            return { ...res.data[key], id: key }
          })
          dispatch(setTodos(result))
        }
      })
  }

  const onSearch = async (title) => {
    if (title.length > 0) {
      dispatch(searchTodo(title))
    } else {
      getTodos()
    }
  }

  const setTodo = async (title) => {
    let order = todos.length + 1
    const todo = { title, done: false, todos: [], order }
    await axios.post(`${url}/todos.json`, todo)
      .then(() => dispatch(addTodo(todo))).then(() => getTodos())
  }

  const onRemove = async id => {
    await axios.delete(`${url}/todos/${id}.json`)
      .then(() => dispatch(removeTodo(id)))
  }

  const onDone = async todo => {
    const item = { ...todo, done: !todo.done }
    await axios.put(`${url}/todos/${todo.id}.json`, item)
      .then(() => dispatch(doneTodo(todo)))
  }

  const updateTitle = async (todo, title) => {
    const item = { ...todo, title: title }
    await axios.put(`${url}/todos/${todo.id}.json`, item)
      .then((res) => dispatch(titleTodo(item)))
  }

  const onSubTodo = async (todo, title) => {
    const item = { ...todo, todos: [...todo?.todos || [], { title: title }] }
    await axios.put(`${url}/todos/${todo.id}.json`, item)
      .then((res) => dispatch(addSubTodo(item)))
  }

  const onRemoveSubTodo = async (todo, key) => {
    const item = { ...todo, todos: todo?.todos?.filter((i, k) => k !== key) }
    await axios.put(`${url}/todos/${todo.id}.json`, item)
      .then((res) => dispatch(removeSubTodo(item)))
  }

  const onMeta = async (todo, title) => {
    const item = { ...todo, metas: [...todo?.metas || [], { title: title }] }
    await axios.put(`${url}/todos/${todo.id}.json`, item)
      .then((res) => dispatch(addMeta(item)))
  }

  const onRemoveMeta = async (todo, key) => {
    const item = { ...todo, metas: todo?.metas?.filter((i, k) => k !== key) }
    await axios.put(`${url}/todos/${todo.id}.json`, item)
      .then((res) => dispatch(removeMeta(item)))
  }

  // Drag & Drop
  const dragStartHendler = async (e, todo) => {
    console.log('drag', todo)
    setCurrent(todo)
  }

  const dragEndHandler = async (e) => {
    e.target.style.background = '#fff'
  }

  const dragOverHandler = async (e) => {
    e.preventDefault()
    e.target.style.background = '#ccc'
  }

  const dropHandler = async (e, todo) => {
    e.preventDefault()

    await todos.map(item => {
      if (item.order === todo.order) {
        axios.put(`${url}/todos/${item.id}.json`, { ...item, order: current.order })
      }
      if (item.order === current.order) {
        axios.put(`${url}/todos/${item.id}.json`, { ...item, order: todo.order })
      }
    })

    dispatch(dragAndDrop({ todo, current }))
    e.target.style.background = '#fff'
  }

  const sortList = (a, b) => { return a.order > b.order ? 1 : -1 }

  return (
    <div className="container py-3">

      <div className="row">
        <div className="col-md-12 mb-3">
          <h3>{title}</h3>
        </div>
        {/* <div className="col-md-6 mb-2">
          <label className="mb-2">Название проектра</label>
          <Form onSubmit={setTitle} placeholder="Введите название проекта" className="border border-1 rounded-3" />
        </div> */}
        <div className="col-md-6 mb-2">
          <label className="mb-2">Добавить задачу</label>
          <Form onSubmit={setTodo} placeholder="Введите название задачи" className="border border-1 rounded-3" />
        </div>
        <div className="col-md-6 mb-2">
          <label className="mb-2">Поиск</label>
          <Form onSubmit={onSearch} onHandler={onSearch} placeholder="Введите название (мин 3 символа)" className="border border-1 rounded-3" />
        </div>
      </div>

      <hr />

      {todos?.length > 0 &&
        <Todos
          todos={todos}
          onRemove={onRemove}
          onDone={onDone}
          updateTitle={updateTitle}
          onSubTodo={onSubTodo}
          onRemoveSubTodo={onRemoveSubTodo}
          onMeta={onMeta}
          onRemoveMeta={onRemoveMeta}
          dragStartHendler={dragStartHendler}
          dragEndHandler={dragEndHandler}
          dragOverHandler={dragOverHandler}
          dropHandler={dropHandler}
          sortList={sortList}
        />}
    </div>
  );
}

export default App;
