import { combineReducers } from "@reduxjs/toolkit"
// import alert from './alertSlice'
import list from './listSlice'

const reducers = combineReducers({
    // alert,
    list,
})

export default reducers