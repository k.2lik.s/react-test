import { createSlice } from "@reduxjs/toolkit";

const list = createSlice({
  name: "list",
  initialState: [],
  reducers: {
    setTodos: (state, action) => {
      return state = Object.assign([], state, action.payload)
    },
    addTodo: (state, action) => {
      return state = [...state, action.payload]
    },
    removeTodo: (state, action) => {
      return state = state.filter(todo => todo.id !== action.payload)
    },
    doneTodo: (state, action) => {
      return state = state.map(todo => todo.id === action.payload.id ? { ...todo, done: !todo.done } : { ...todo })
    },
    titleTodo: (state, action) => {
      return state = state.map(todo => todo.id === action.payload.id ? { ...todo, title: action.payload.title } : { ...todo })
    },
    addSubTodo: (state, action) => {
      return state = state.map(todo => todo.id === action.payload.id ? { ...todo, todos: action.payload.todos } : { ...todo })
    },
    removeSubTodo: (state, action) => {
      return state = state.map(todo => todo.id === action.payload.id ? { ...todo, todos: action.payload.todos } : { ...todo })
    },
    addMeta: (state, action) => {
      return state = state.map(todo => todo.id === action.payload.id ? { ...todo, metas: action.payload.metas } : { ...todo })
    },
    removeMeta: (state, action) => {
      return state = state.map(todo => todo.id === action.payload.id ? { ...todo, metas: action.payload.metas } : { ...todo })
    },
    searchTodo: (state, action) => {
      return state = state.filter(todo => todo.title.indexOf(action.payload) > -1)
    },
    dragAndDrop: (state, action) => {
      let current = action.payload.current
      let next = action.payload.todo
      return state = state.map(item => {
        if (item.id === next.id) {
          return { ...item, order: current.order }
        }
        if (item.id === current.id) {
          return { ...item, order: next.order }
        }
        return item
      })
    },
  }
})

const { actions, reducer } = list;
export const {
  addTodo,
  setTodos,
  removeTodo,
  doneTodo,
  titleTodo,
  addSubTodo,
  removeSubTodo,
  addMeta,
  removeMeta,
  searchTodo,
  dragAndDrop
} = actions
export default reducer